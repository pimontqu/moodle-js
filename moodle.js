$(function(){
    // Quand l'iframe est complètement chargé
    $('iframe').on('load', function(){
        // contioendra tous les infos de la page de l'iframe
        var json = {};
        // la date et l'heure actuelle
        var currentdate = new Date();
        // récupère le nom de l'utilisateur
        var user_name = document.getElementsByClassName('usertext')[0].textContent;
        // le fil d'ariane du chemin moodle
        var ariane_wire = document.getElementsByClassName('breadcrumb')[0];
        // on separe les liens dans un tableau
        var ariane_wire_split = ariane_wire.getElementsByClassName('breadcrumb-item');
        // contiendras tous les lien au format json
        var json_path_to_page = [];
        // pour chaque liens
        for(item = 0; item < ariane_wire_split.length; item++){
            // s'il y à un lien
            if(ariane_wire_split[item].childElementCount > 0){
                // on le met en format json (lien et son nom) puis on l'ajoute au tableau
                json_path_to_page.push({'name' : ariane_wire_split[item].textContent, 'url' : ariane_wire_split[item].firstChild.href});
            }
        }
        // contiendras le chemin popre au page de l'iframe
        var path_to_page_iframe = []
        // récupèration de l'iframe
        var iframe = document.getElementById('resourceobject');
        // récupère le lien vers la page d'accueil
        var a_home = iframe.contentWindow.document.querySelector('nav.headingSelector ul.mnu li.home div.lbl a.item');
        // si on est pas sur la page d'acceuil
        if (a_home != null){
            // le titre de la page d'acceuil
            var title_home = iframe.contentWindow.document.querySelector('#header h1').textContent;
            // liens vers la page d'acceuil
            var href_home = a_home.href;
            // ajout de ces informations dans un tableau aux format json
            path_to_page_iframe.push({'name' : title_home, 'url' : href_home});
        }
        // url de la page courrante
        var iframe_url = iframe.contentDocument.URL;
        // tire de la page courante
        var title_iframe = iframe.contentWindow.document.getElementsByClassName('hBk_ti')[0].textContent;
        // récupère la position dans le menu de navigation
        var section_select = iframe.contentWindow.document.getElementsByClassName('sel_yes');
        // si on n'est pas sur la page d'acceuil
        if(section_select.length > 0){
            // parent de la position dans le menu de navigation
            var section_select_parent = section_select[0].parentElement;
            // classe(s) du parent
            var section_select_parent_class = section_select_parent.className;
            // si les classes du parent sont égale sub mnu_open
            if(section_select_parent_class === 'sub mnu_open'){
                // élément html de la section supèrieure
                var up_section = section_select_parent.parentElement;
                // nom de la catègorie supèrieure
                var up_section_text = up_section.getElementsByClassName('item')[0].textContent;
                // url de la section supèrieure
                var up_section_url = up_section.getElementsByClassName('item')[0].href;
                // ajout des infos de la catègorie supière aux format json
                path_to_page_iframe.push({'name' : up_section_text, 'url' : up_section_url});
            }
        }
        // ajout des infos de la page courrent au format json
        path_to_page_iframe.push({'name' : title_iframe, 'url' : iframe_url});
        // création du json final
        // ajout de la date
        json['date'] = currentdate;
        // ajout de l'utilsateur
        json['user'] = user_name;
        // ajout du chemin de moodle
        json['moodle_path'] = json_path_to_page;
        // ajout du chemin poppre à l'iframe
        json['iframe_path'] = path_to_page_iframe;
        console.log(json);
    });
});